package me.martijnpu.prefix.Paper;

import me.martijnpu.prefix.Bukkit.BukkitStatics;
import me.martijnpu.prefix.EventHandler.PlaceholderAPI;
import me.martijnpu.prefix.FileHandler.Config.ConfigData;
import me.martijnpu.prefix.FileHandler.Messages;
import me.martijnpu.prefix.LuckPermsConnector;
import me.martijnpu.prefix.Util.Interfaces.*;
import me.martijnpu.prefix.Util.Statics;
import org.bstats.bukkit.Metrics;
import org.bstats.charts.SimplePie;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements IPrefix, Listener {
    private static Main instance;

    public static Main get() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        Statics.isProxy = false;
        PrefixAdapter.setAdapter(this);
        Statics.setCurrVersion(getDescription().getVersion());

        try {
            if (Integer.parseInt("%%__RESOURCE__%%") == 70359)
                Messages.PLUGIN.sendConsole("&aThanks for downloading");
        } catch (NumberFormatException ignore) {
        }

        if (LuckPermsConnector.checkInvalidVersion()) {
            this.setEnabled(false);
            return;
        }
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, this::checkForUpdates, 0, 1728000);

        PaperFileManager.getInstance().printInitData();
        ConfigData.getInstance().printInitData();
        new PaperCommand();
        new PaperPlayerJoin();

        if (ConfigData.BSTATS.get()) {
            Metrics metrics = new Metrics(this, 10920);
            metrics.addCustomChart(new SimplePie("luckpermsVersion", LuckPermsConnector::getLPVersion));
            metrics.addCustomChart(new SimplePie("language", ConfigData.LOCALE::get));
        }

        if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null)
            PlaceholderAPI.registerPAPI(); //No message needed since PAPI is already sending a register message
        else
            Messages.PLUGIN.sendConsole("&7No PAPI found to hook into. Disabling placeholder support...");

        Messages.PLUGIN.sendConsole("&aWe're up and running");
    }

    @Override
    public void onDisable() {
        Messages.PLUGIN.sendConsole("&aDisabled successful");
    }

    private void checkForUpdates() {
        Bukkit.getScheduler().runTaskAsynchronously(this, Statics::checkForUpdate);
    }

    @Override
    public IMessage getMessagesAdapter() {
        return PaperMessages.getInstance();
    }

    @Override
    public IConfig getConfigAdapter() {
        return PaperFileManager.getInstance();
    }

    @Override
    public IStatic getStaticsAdapter() {
        return BukkitStatics.getInstance();
    }
}
