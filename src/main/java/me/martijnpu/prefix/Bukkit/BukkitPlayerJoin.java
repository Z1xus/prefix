package me.martijnpu.prefix.Bukkit;

import me.martijnpu.prefix.Util.Statics;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class BukkitPlayerJoin implements Listener {
    BukkitPlayerJoin() {
        Main.get().getServer().getPluginManager().registerEvents(this, Main.get());
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Statics.onPlayerJoin(e.getPlayer());
    }
}
