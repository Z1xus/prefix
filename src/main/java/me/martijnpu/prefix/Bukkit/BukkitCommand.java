package me.martijnpu.prefix.Bukkit;

import me.martijnpu.prefix.FileHandler.Messages;
import me.martijnpu.prefix.Util.Interfaces.ICommand;
import org.bukkit.command.*;
import org.bukkit.entity.Player;

import java.util.List;

class BukkitCommand extends ICommand implements CommandExecutor, TabCompleter {
    BukkitCommand() {
        PluginCommand commandPrefiX = Main.get().getCommand("prefix");
        if (commandPrefiX != null) {
            commandPrefiX.setExecutor(this);
            commandPrefiX.setTabCompleter(this);
        } else
            Messages.WARN.sendConsole("Unable to register prefix commands...");

        PluginCommand commandSuffiX = Main.get().getCommand("suffix");
        if (commandSuffiX != null) {
            commandSuffiX.setExecutor(this);
            commandSuffiX.setTabCompleter(this);
        } else
            Messages.WARN.sendConsole("Unable to register suffix commands...");

        PluginCommand commandTemplate = Main.get().getCommand("prefixtemplate");
        if (commandTemplate != null) {
            commandTemplate.setExecutor(this);
            commandTemplate.setTabCompleter(this);
        } else
            Messages.WARN.sendConsole("Unable to register prefix template commands...");
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String alias, String[] args) {
        if (cmd.getName().equalsIgnoreCase("prefix"))
            return onPrefixTabComplete(sender, args);
        if (cmd.getName().equalsIgnoreCase("suffix"))
            return onSuffixTabComplete(sender, args);
        if (cmd.getName().equalsIgnoreCase("prefixtemplate"))
            return onTemplateTabComplete(sender, args);
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String alias, String[] args) {
        if (cmd.getName().equalsIgnoreCase("prefix"))
            return onPrefixCommand(sender, (sender instanceof Player), args);
        if (cmd.getName().equalsIgnoreCase("suffix"))
            return onSuffixCommand(sender, (sender instanceof Player), args);
        if (cmd.getName().equalsIgnoreCase("prefixtemplate"))
            return onTemplateCommand(sender, (sender instanceof Player), args);
        return false;
    }
}
