package me.martijnpu.prefix.Bukkit;

import me.martijnpu.prefix.Util.Interfaces.IStatic;
import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class BukkitStatics implements IStatic {
    private static BukkitStatics instance;

    private BukkitStatics() {
    }

    public static BukkitStatics getInstance() {
        if (instance == null)
            instance = new BukkitStatics();
        return instance;
    }

    @Override
    public String getServerVersion() {
        return Bukkit.getVersion();
    }

    @Override
    public List<String> getOnlinePlayers() {
        return Bukkit.getOnlinePlayers().stream().map(HumanEntity::getName).collect(Collectors.toList());
    }

    @Override
    public String getDisplayName(Object player) {
        if (player instanceof Player)
            return ((Player) player).getDisplayName();
        else
            return "Console";
    }

    @Override
    public UUID getUUID(Object sender) {
        if (sender instanceof Player)
            return ((Player) sender).getUniqueId();
        else
            return null;
    }

    @Override
    public Object getPlayer(String name) {
        return Bukkit.getPlayer(name);
    }
}
