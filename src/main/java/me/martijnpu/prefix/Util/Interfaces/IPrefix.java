package me.martijnpu.prefix.Util.Interfaces;

public interface IPrefix {
    IMessage getMessagesAdapter();

    IConfig getConfigAdapter();

    IStatic getStaticsAdapter();
}
