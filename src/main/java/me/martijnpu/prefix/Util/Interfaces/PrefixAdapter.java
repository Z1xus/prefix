package me.martijnpu.prefix.Util.Interfaces;

public class PrefixAdapter {
    private static IPrefix adapter;

    public static void setAdapter(IPrefix adapter) {
        PrefixAdapter.adapter = adapter;
    }

    public static IMessage getMessagesAdapter() {
        return adapter.getMessagesAdapter();
    }

    public static IConfig getConfigAdapter() {
        return adapter.getConfigAdapter();
    }

    protected static IStatic getStaticsAdapter() {
        return adapter.getStaticsAdapter();
    }
}
