package me.martijnpu.prefix.Util.Interfaces;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;

public interface IMessage {
    void sendConsoleWarning(String message);

    void sendConsole(String message);

    String getMessage(String path);

    void sendBigMessage(Object sender, TextComponent message);

    void sendMessage(Object sender, String message);

    BaseComponent[] convert(String old);
}
