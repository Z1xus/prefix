package me.martijnpu.prefix.Util;

import net.luckperms.api.context.ImmutableContextSet;

public class LuckPermsResult {
    public String tag;
    public ImmutableContextSet contextSet;

    public LuckPermsResult(String tag) {
        this.tag = tag;
        contextSet = null;
    }
}
