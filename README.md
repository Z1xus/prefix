<!-- General links & pictures -->
[Discord Server]: https://discord.gg/2RHYvNy "Join the Discord Server"
[Discord Logo]: https://icons.iconarchive.com/icons/papirus-team/papirus-apps/256/discord-icon.png "Join the Discord Server"
[LuckPerms]: https://luckperms.net "LuckPerms Homepage"
[DeluxeMenus]: https://www.spigotmc.org/resources/deluxemenus.11734/ "DeluxeMenus"
[bStats]: https://bstats.org/plugin/bukkit/PrefiX/10920 "bStats PrefiX"
[bStats-picture]: https://bstats.org/signatures/bukkit/PrefiX.svg "bStats PrefiX"
[SpigotMC]: https://www.spigotmc.org/resources/prefix.70359/ "SpigotMC PrefiX"
[GitLab-Languages]: https://gitlab.com/martijnpu/prefix/-/tree/master/src/main/resources/languages "Available Languages"

<!-- icons -->
[spigot-downloads]: https://img.shields.io/spiget/downloads/70359?label=Downloads
[spigot-version]: https://img.shields.io/spiget/version/70359?label=Version
[spigot-rating]: https://img.shields.io/spiget/stars/70359?label=Rating
[spigot-versions]: https://img.shields.io/spiget/tested-versions/70359?label=Supported%20Versions
[bStats-players]: https://img.shields.io/bstats/players/10920?label=Online%20Players
[bStats-servers]: https://img.shields.io/bstats/servers/10920?label=Online%20servers
[discord-online]: https://img.shields.io/discord/755734170709524557?label=%7F&logo=discord&logoColor=%235865F2
[languages]: https://img.shields.io/badge/Languages-4-green
<!-- End of variables -->

<h1><center><strong><a href="https://hangar.papermc.io/martijnpu/PrefiX" title="PrefiX Home page">PrefiX</a></strong></center></h1>
<center>
  <a href="https://hangar.papermc.io/martijnpu/PrefiX/pages/Documentation/Commands" title="PrefiX Commands page">Commands</a> - 
  <a href="https://hangar.papermc.io/martijnpu/PrefiX/pages/Documentation/Permissions" title="PrefiX Permissions page">Permissions</a> - 
  <a href="https://hangar.papermc.io/martijnpu/PrefiX/pages/Documentation/Config" title="PrefiX Config page">Config</a> - 
  <a href="https://hangar.papermc.io/martijnpu/PrefiX/pages/Documentation/Languages" title="PrefiX Languages page">Languages</a> - 
  <a href="https://hangar.papermc.io/martijnpu/PrefiX/pages/Documentation/Placeholders" title="PrefiX Placeholders page">Placeholders</a>
</center>

[![spigot-downloads]][SpigotMC] [![spigot-version]][SpigotMC] [![spigot-rating]][spigotMC] [![spigot-versions]][SpigotMC] [![bStats-players]][bStats] [![bStats-servers]][bStats] [![discord-online]][Discord Server]
[![languages]][GitLab-Languages]


## **Description**
PrefiX is an easy and clean plugin to support in editing the Prefixes and Suffixes on your server.<br>
Players can adapt their Prefix and/or Suffix based on their given permissions. Easy to configure and even easier to use!
PrefiX is build against [LuckPerms].

For quick support ask your questions in our [Discord server]!

## **Features**

- Uses ***only*** LuckPerms for handling and saving
- Colors of the ***prefix***, ***name***, ***bracket*** and ***suffix*** are separated
- ***Blacklist*** specific words
- ***Whitelist*** character groups and custom characters
- Fully ***customizable*** colors and formatting
  - Different permission for each color
  - Formatting toggles
  - All colors available
  - All formatting available
- ***Hexadecimal*** support
  - Hexadecimal names support
  - Custom hexadecimal format
  - Gradient support
- ***Moderating*** for staff
  - You can change others prefix/colors/suffix
- ***Configurable*** characters before and after the prefix
- ***Auto-checker*** for new updates
- ***Overviewlist*** with all available colors for the player
- ***Interactive*** color list
- ***Suffix*** support
- ***BungeeCord*** support
- Define your own **custom messages** *(Or other **languages**)*
- Ability to respect **context** from LuckPerms
- Support for ***spaces*** in the Prefix/Suffix by adding ""
- ***Placeholder*** support via ***PAPI***
- ***DeluxeMenus*** support

Prefix has [DeluxeMenus] support, this means that it's possible to let players choose from a Deluxe Menu a presetted prefix. This will ensure you as admin that only predefined prefixes are chosen. For more information about DeluxeMenus support visit out [Discord Server].

* Option to see all colors available:
  ![https://i.imgur.com/a3WlEn4.png](https://i.imgur.com/a3WlEn4.png)

* Clickable overview of available colors:
  ![https://i.imgur.com/UQBVShp.png](https://i.imgur.com/UQBVShp.png)

## **Setup spigot & bungeecord**
This plugin is just download and drop in your plugins folder.<br>
Please be aware it requires [LuckPerms v5][LuckPerms]  to work.

## **Statics**
This Plugin uses bStats! This submits anonymous stats about your server (player count, plugin/server version, etc.) to the public statistic website [bStats] which will provide developers with usage statistics of this plugin. If you don't want this feature and wish to opt-out, you can add *'bstats: false'* to the config.<br>
It would mean a lot to me though if you could leave it enabled, its part of what keeps me motivated :)

## **Trusted by many people**
PrefiX in stable, easy to use and simple to setup. Therefore it's used and trusted in many servers with great success.
[![bStats-picture]][bStats]

## ***Want to display custom prefixes...***
*...in your chat? You need a chat formatting plugin.*<br>
*...in the tab list? You need a tab plugin.*<br>
*...somewhere else? You need a specific plugin for that.*
<br>
<br>
# **Looking for support?**
Visit our [Discord server] for quick support for all your questions!<br>
## [![Discord Logo]][Discord Server]

**Thanks.**